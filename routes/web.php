<?php

Route::get('/', 'Home@index');

Route::get('add-restaurant', 'Restaurant@index');

Route::get('restaurants-map', 'Restaurant@map');

Route::post('add-restaurant', 'Restaurant@store');

Route::get('load-xml', 'Restaurant@xmlData');
