@include('layouts.header')

<div id="contact">
    <div class="container">
    </div>
    <div class="container">
        <!-- start: Slider -->
        <div class="slider-wrapper">

            <div id="da-slider" class="da-slider">
                <div class="da-slide">
                    <h2>restaurant nearby ?</h2>
                    <div class="da-img"><img src="img/parallax-slider/twitter.png" alt="image01" /></div>
                </div>
                <div class="da-slide">
                    <h2>locate a restaurant</h2>
                    <div class="da-img"><img src="img/parallax-slider/responsive.png" alt="image02" /></div>
                </div>
                <div class="da-slide">
                    <h2>get directions</h2>
                    <div class="da-img"><img src="img/parallax-slider/html5.png" alt="image03" /></div>
                </div>
                <div class="da-slide">
                    <h2>where ever, when ever...</h2>
                    <div class="da-img"><img src="img/parallax-slider/css3.png" alt="image04" /></div>
                </div>
                <nav class="da-arrows">
                    <span class="da-arrows-prev"></span>
                    <span class="da-arrows-next"></span>
                </nav>
            </div>

        </div>
        <!-- end: Slider -->
    </div>
</div>

@include('layouts.footer')
