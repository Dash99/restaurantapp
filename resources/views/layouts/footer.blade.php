<!-- start: Copyright -->
<div id="copyright">

    <!-- start: Container -->
    <div class="container">

        <p>
            &copy; 2017 - Jozi Places - Restaurant Advisor <a href="/" alt="#"></a>
        </p>

    </div>
    <!-- end: Container  -->

</div>
<!-- end: Copyright -->

<!-- start: Java Script -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery-1.8.2.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/flexslider.js"></script>
<script src="js/carousel.js"></script>
<script src="js/jquery.cslider.js"></script>
<script src="js/slider.js"></script>
<script defer="defer" src="js/custom.js"></script>
<!-- end: Java Script -->

</body>
</html>