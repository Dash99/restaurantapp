<?php

use DB;

$dom = new DOMDocument();
$node = $dom->createElement("markers");
$parnode = $dom->appendChild($node);

$result = DB::table('restaurants')->get();

header("Content-type: text/xml");

$count = 0;
while (empty($result[$count]) === FALSE){

    $node = $dom->createElement("marker");
    $newnode = $parnode->appendChild($node);
    $newnode->setAttribute("name",$result[$count]->name);
    $newnode->setAttribute("address", $result[$count]->address);
    $newnode->setAttribute("telephone", $result[$count]->telephone);
    $newnode->setAttribute("lat", $result[$count]->lat);
    $newnode->setAttribute("lng", $result[$count]->lng);

    $count++;
}

echo $dom->saveXML();
