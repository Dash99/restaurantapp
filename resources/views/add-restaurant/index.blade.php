@include('layouts.header')
<div id="contact">
    <div class="container">
        <div class="title">
            <h3>Add Restaurant</h3>
        </div>
    </div>
    <br><br>
    <div class="container">
        <div class="row">
            <form action="{{url('add-restaurant')}}" method="post" role="form" class="contactForm"
                  enctype="multipart/form-data">
                <div class="span4">
                    <div class="col-md-4">
                        <div class="form-group">
                            Name : <br>
                            <input required type="text" class="form-control" name="name" placeholder="Restaurant name"
                                   data-rule="name" data-msg="Please enter a valid name"/>
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            Telephone : <br>
                            <input required type="text" name="telephone" class="form-control"
                                   placeholder="Telephone No"
                                   data-rule="minlen:4" data-msg="Please enter valid number"/>
                            <div class="validation"></div>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="col-md-4">
                        <div class="form-group">
                            Address : <br>
                            <input type="text" required class="form-control" name="address" id="autocomplete"
                                   placeholder="Enter your address" onFocus="geolocate()"/>
                            <input type="hidden" class="form-control" id="street_number" disabled="true"/>
                            <input type="hidden" class="form-control" id="route" disabled="true"/>
                            <input type="text" class="form-control" id="locality" disabled="true"/>
                            <input type="hidden" class="form-control" id="administrative_area_level_1" disabled="true"/>
                            <input type="hidden" class="form-control" id="postal_code" disabled="true"/>
                            <input type="text" class="form-control" id="country" disabled="true"/>
                            <input type="hidden" name="lat" id="lat"/>
                            <input type="hidden" name="lng" id="lng"/>
                            <div class="validation"></div>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <button type="submit" class="btn btn-theme pull-left btn-primary btn-lg"
                                    style="width: 100%;">
                                Add Restaurant
                            </button>
                        </div>

                    </div>
                </div>
                {!! csrf_field() !!}
            </form>
        </div>
    </div>
</div>
<br><br>

@include('layouts.footer')

<!-- Google Maps javascript API : autocomplete address form -->

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9gQaBW0gX0wEpeq7JiMdWnVrkZ56pdQA&libraries=places&callback=initAutocomplete"
        async defer>
</script>
<script>
    var placeSearch, autocomplete;

    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('autocomplete')),
            {types: ['geocode']});

        autocomplete.addListener('place_changed', fillInAddress);


    }

    function fillInAddress() {
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }

        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }

        var address = document.getElementById('autocomplete').value;
        getPosition(address);
    }

    function getPosition(address) {
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {
                lat =  results[0].geometry.location.lat();
                lng = results[0].geometry.location.lng();
                document.getElementById('lat').setAttribute('value',lat);
                document.getElementById('lng').setAttribute('value',lng);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

</script>
