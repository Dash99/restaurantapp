@include('layouts.header')

<div id="contact" class="map-canvas">
    <div class="container">
        <div id="floating-panel" align="middle">
            <input type="hidden" id="start" value="" disabled>
            <strong>get Directions to :</strong><br>
            <select id="end">
                <?php
                $result = DB::table('restaurants')->get()->where('id', '>', 0);
                $count = 0;
                while (empty($result[$count]) === FALSE) {
                    echo '<option value="' . $result[$count]->address . '">' . $result[$count]->name . '</option>';
                    $count++;
                }
                ?>
            </select>
        </div>

        <div class="row">
            <strong>{{$msg}}</strong>
            <div class="col-md-4">
                <div id="map"></div>
                <div id="right-panel"></div>
            </div>
        </div>

    </div>
</div>

@include('layouts.footer')

<!-- Google Maps js api : geolocation, geocoder, directions   -->
<script defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBynDyejG6_Jnk94qbo98jwa7mMRF7vpxU&callback=initMap">
</script>
<script>
    function initMap() {

        var markers = [];

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10
        });


        var infoWindow = new google.maps.InfoWindow({map: map});
        if (navigator.geolocation) {

            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                getUserAddress(pos.lat, pos.lng);

                infoWindow.setPosition(pos);
                infoWindow.setContent('You are here.');

                map.setCenter(pos);

            }, function () {
                handleLocationError(true, infoWindow, map.getCenter());
            });

        } else {
            handleLocationError(false, infoWindow, map.getCenter());
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            /*infoWindow.setContent(browserHasGeolocation ?
             'Error: The Geolocation service failed.' :
             'Error: Your browser doesn\'t support geolocation.');*/
        }

        function getUserAddress(lat, lng) {
            var geocoder = new google.maps.Geocoder();
            var location = new google.maps.LatLng(lat, lng);
            geocoder.geocode({'latLng': location}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var address = results[0].formatted_address;
                    document.getElementById('start').setAttribute('value', address);
                }
            });
        }

        downloadUrl('/xml.php', function (data) {

            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');

            Array.prototype.forEach.call(markers, function (markerElem) {
                var name = markerElem.getAttribute('name');
                var address = markerElem.getAttribute('address');
                var telephone = markerElem.getAttribute('telephone');
                var lat = markerElem.getAttribute('lat');
                var lng = markerElem.getAttribute('lng');

                var infowincontent = document.createElement('div');
                var strong = document.createElement('strong');
                strong.textContent = name
                infowincontent.appendChild(strong);
                infowincontent.appendChild(document.createElement('br'));

                var text = document.createElement('text');
                text.textContent = address
                infowincontent.appendChild(text);

                geocodeAddress(lat, lng, address, map, name, telephone);

            });

        });

        var directionsDisplay = new google.maps.DirectionsRenderer;
        var directionsService = new google.maps.DirectionsService;

        directionsDisplay.setMap(map);
        directionsDisplay.setPanel(document.getElementById('right-panel'));

        var control = document.getElementById('floating-panel');
        control.style.display = 'block';
        map.controls[google.maps.ControlPosition.TOP_CENTER].push(control);

        var onChangeHandler = function () {
            calculateAndDisplayRoute(directionsService, directionsDisplay);
        };

        document.getElementById('start').addEventListener('change', onChangeHandler);
        document.getElementById('end').addEventListener('change', onChangeHandler);
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var geocoder = new google.maps.Geocoder();
        var start = document.getElementById('start').value;//getUserAddress(geocoder);
        var end = document.getElementById('end').value;
        directionsService.route({
            origin: start,
            destination: end,
            travelMode: 'DRIVING'
        }, function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }


    function getUserAddress(geocoder) {
        geocoder.geocode({'location': userPos}, function (results, status) {
            var position;
            if (status === 'OK') {
                return results[0].geometry.location;
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    function geocodeAddress(lat, lng, address, map, name, telephone) {

        var marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(lat, lng),
            title: name
        });

        var contentString = '<div id="content">' +
            '<div id="siteNotice"></div>' +
            '<h1 id="firstHeading" class="firstHeading">' + name + '</h1>' +
            '<h3> Restaurant </h3>' +
            '<div id="bodyContent">' +
            '<p><strong>Address :</strong> <br>' + address + '</p>' +
            '<p><strong>Tel :</strong> <br>' + telephone + '</p>' +
            '</div>' +
            '</div>';

        var infoWindow = new google.maps.InfoWindow({
            content: contentString
        })

        marker.addListener('click', function () {
            infoWindow.open(map, marker)
        });
    }

    function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                request.onreadystatechange = doNothing();
                callback(request, request.status);
            }
        };

        request.open('GET', url, true);
        request.send(null);
    }

    function doNothing() {
    }
</script>
