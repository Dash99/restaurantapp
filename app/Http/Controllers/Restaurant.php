<?php

namespace App\Http\Controllers;

use App\Models\Restaurant as Model;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use DB;

class Restaurant extends Controller
{
    public function index()
    {
        return view('add-restaurant.index');
    }

    public function map(){
        return view('restaurants-map.index')->with('msg','.');
    }

    public function xmlData()
    {
        return view('data.xml');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'telephone' => 'required',
            'address' => 'required',
            'lat' => 'required',
            'lng' => 'required'
        ]);

        try {
            $restaurant = $request->all();
            Model::create($restaurant);
            return view('restaurants-map.index')->with('msg','added successfully');
        } catch (Exception $exception) {
            return $exception->getCode() . ' : ' . $exception->getMessage();
        }
    }

    public function show()
    {
        Model::show(c);
    }
}
